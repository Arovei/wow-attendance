﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System.IO;
using System.Threading;

namespace WoWAttendance
{
    class Program
    {
        static string[] Scopes = { SheetsService.Scope.Spreadsheets };
        static string ApplicationName = "Friends Attendance Test";

        static void Main(string[] args)
        {
            Console.Write("Input log code to read from: ");
            string input = Console.ReadLine();
            var x = WclRequest.wcl(input);
            List<string> fightCheck = new List<string>();
            List<string> fightCheckName = new List<string>();
            List<string> MCBossNames = new List<string>() { "Lucifron", "Magmadar", "Gehennas", "Garr", "Baron", "Shazzrah", "Sulfuron", "Golemagg", "Majordomo", "Ragnaros" };
            List<string> BWLBossNames = new List<string>() { "Razorgore", "Vaelastrasz", "Broodlord", "Firemaw", "Ebonroc", "Flamegor", "Chromaggus", "Nefarian" };
            List<string> ZGBossNames = new List<string>() { "Venoxis", "Jeklik", "Mar'li", "Thekal", "Arlokk", "Hakkar", "Mandokir", "Madness", "Ghaz'ranka", "Hexxer" };
            List<string> RAQBossNames = new List<string>() { "Kurinnaxx", "General", "Moam", "Buru", "Ayamiss", "Ossirian" };
            List<string> AQBossNames = new List<string>() { "Skeram", "Sartura", "Fankriss", "Huhuran", "Emperors", "C'Thun", "Trio", "Viscidus", "Ouro" };
            List<string> NaxxBossNames = new List<string>() { "Anub'Rekhan", "Faerlina", "Maexxna", "Noth", "Heigan", "Loatheb", "Razuvious", "Gothik", "Horsemen", "Patchwerk", "Grobbulus", "Gluth", "Thaddius", "Sapphiron", "Kel'Thuzad" };

            SortedDictionary<string, List<string>> playerInfo = new SortedDictionary<string, List<string>>();
            long totalFights = 0;

            for (int i = 0; i < x.Result.Fights.Count; i++)
            {
                //Cycle through the fight result info, load into an array for cross-referencing attendance
                var y = x.Result.Fights[i];
                totalFights = x.Result.Fights.Count;
                //If kill == null, it means it was a trash fight (So far)
                //ATM only giving EP for successful kills
                if (y.Kill == true)
                {
                    //Add the ID to cross reference with player
                    fightCheck.Add(y.Id.ToString());
                    //Add the Fight name so specific ones may be added if not whole raid; also some are truncated
                    if (x.Result.Zone == 1000)
                    {
                        //Molen Core
                        fightCheck.Add(y.Name.Split()[0]);
                    }
                    else if (x.Result.Zone == 1002)
                    {
                        //Blackwing Lair
                        fightCheck.Add(y.Name.Split()[0]);
                    }
                    else if (x.Result.Zone == 0)
                    {
                        //Zul'Gurub
                        fightCheck.Add(y.Name.Split().Last());
                    }
                    else if (x.Result.Zone == 0)
                    {
                        //Ruins of Ahn'Qiraj
                        fightCheck.Add(y.Name.Split()[0]);
                    }
                    else if (x.Result.Zone == 0)
                    {
                        //Ahn'Qiraj
                        if (y.Name == "Fankriss the Unyielding") { fightCheck.Add(y.Name.Split()[0]); }
                        else { fightCheck.Add(y.Name.Split().Last()); }
                    }
                    else if (x.Result.Zone == 0)
                    {
                        //Naxxramas
                        if (y.Name == "Grand Widow Faerlina") { fightCheck.Add(y.Name.Split().Last()); }
                        else if (y.Name == "Instructor Razuvious") { fightCheck.Add(y.Name.Split().Last()); }
                        else if (y.Name == "The Four Horsemen") { fightCheck.Add(y.Name.Split().Last()); }
                        else { fightCheck.Add(y.Name.Split()[0]); }
                    }
                    else
                    {
                        fightCheck.Add(y.Name);
                    }
                }
            }
            for (int i = 0; i < x.Result.Friendlies.Count; i++)
            {
                List<string> bossFight = new List<string>();
                var y = x.Result.Friendlies[i];
                if (y.Type != "Pet" && y.Type != "Boss")
                {
                    for (int j = 0; j < totalFights + 1; j++)
                    {
                        //If The ID of this fight for the current player matches a boss fight, add to attendance
                        if (j < y.Fights.Count)
                        {
                            if (fightCheck.Contains(y.Fights[j].Id.ToString()))
                            {
                                int location = fightCheck.IndexOf(y.Fights[j].Id.ToString());
                                bossFight.Add(fightCheck[location + 1]);
                            }
                        }
                    }
                    if (bossFight.FirstOrDefault() == "Onyxia")
                    {
                        //Do nooothingggggg, but also don't do anything down there cuz that'll fuck it all up n stuff
                    }
                    else if ((bossFight.Count * 2) == fightCheck.Count)
                    {
                        bossFight.Clear();
                        if (fightCheck.Contains("Ragnaros")) { bossFight.Add("Molten Core"); }
                        else if (fightCheck.Contains("Nefarian")) { bossFight.Add("Blackwing Lair"); }
                        else if (fightCheck.Contains("Hakkar")) { bossFight.Add("Zul'Gurub"); }
                        else if (fightCheck.Contains("C'Thun")) { bossFight.Add("Ahn'Qiraj"); }
                        else if (fightCheck.Contains("Ossirian")) { bossFight.Add("Ruins of Ahn'Qiraj"); }
                        else if (fightCheck.Contains("Kel'Thuzad")) { bossFight.Add("Naxxramas"); }
                    }
                    else
                    {
                        for (int k = 0; k < bossFight.Count; k++)
                        {
                            if (MCBossNames.Contains(bossFight[k]))
                            {
                                bossFight[k] = "MC " + bossFight[k];
                            }
                            else if (BWLBossNames.Contains(bossFight[k]))
                            {
                                bossFight[k] = "BWL " + bossFight[k];
                            }
                            else if (ZGBossNames.Contains(bossFight[k]))
                            {
                                bossFight[k] = "ZG " + bossFight[k];
                            }
                            else if (RAQBossNames.Contains(bossFight[k]))
                            {
                                bossFight[k] = "RAQ " + bossFight[k];
                            }
                            else if (AQBossNames.Contains(bossFight[k]))
                            {
                                bossFight[k] = "AQ " + bossFight[k];
                            }
                            else if (NaxxBossNames.Contains(bossFight[k]))
                            {
                                bossFight[k] = "Naxx " + bossFight[k];
                            }
                        }
                    }
                    playerInfo.Add(y.Name, bossFight);
                }

            }
            // Needed for 
            List<string> uploadPlayers = new List<string>();
            List<string> uploadBosses = new List<string>();
            List<string> uploadTime = new List<string>();
            foreach (var name in playerInfo)
            {
                foreach (var boss in name.Value)
                {
                    uploadPlayers.Add(name.Key);
                    uploadBosses.Add(boss);
                    uploadTime.Add(DateTime.Today.ToShortDateString());
                }
            }


            //Here starts the Upload to Google Sheets
            UserCredential credential;

            using (var stream =
                new FileStream("credentials.json", FileMode.Open, FileAccess.Read))
            {
                // The file token.json stores the user's access and refresh tokens, and is created
                // automatically when the authorization flow completes for the first time.
                string credPath = "token.json";
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                Console.WriteLine("Credential file saved to: " + credPath);
            }

            // Create Google Sheets API service.
            var service = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });

            // Define request parameters.
            String spreadsheetId = "1PPBQhVlttTO8VGDKLgFsnYvZE5XaiGTqpAaqabY_RBo";
            //String spreadsheetId = "1d4xjIEfSNf2-knGDyeYLvbkWW6NoNLCGAN0awTctMzw";
            string activeSheet = "Effort";

            SpreadsheetsResource.ValuesResource.GetRequest ssinfo = service.Spreadsheets.Values.Get(spreadsheetId, activeSheet+"!A1:A");
            //SpreadsheetsResource.ValuesResource.GetRequest ssinfo = service.Spreadsheets.Values.Get(spreadsheetId, activeSheet+"!A1:A");
            ValueRange lastRowUsed = ssinfo.Execute();

            ValueRange upload = new ValueRange();
            upload.MajorDimension = "COLUMNS";//"ROWS";//COLUMNS

            for (int i = 0; i < 3; i++)
            {
                var lru = 1;
                if (lastRowUsed.Values != null) { lru = lastRowUsed.Values.Count + 1; }
                
                String range = activeSheet+"!A" + lru;
                
                var oblist = new List<object>();
                if (i == 0) { foreach (var item in uploadPlayers) { oblist.Add(item); } }
                else if (i == 1) { foreach (var item in uploadBosses) { oblist.Add(item); } range = activeSheet+"!B" + lru; }
                else if (i == 2) { foreach (var item in uploadTime) { oblist.Add(item); } range = activeSheet+"!C" + lru; }
                upload.Values = new List<IList<object>> { oblist };                
                SpreadsheetsResource.ValuesResource.UpdateRequest update = service.Spreadsheets.Values.Update(upload, spreadsheetId, range);
                update.ValueInputOption = SpreadsheetsResource.ValuesResource.UpdateRequest.ValueInputOptionEnum.RAW;
                UpdateValuesResponse result2 = update.Execute();
            }
            Console.WriteLine("Update completed.");

            Console.ReadLine();
        }
    }
}
