﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace WoWAttendance
{
    public class WarcraftLog
    {
        public WarcraftLog(string json)
        {

        }
        public string fights { get; set; }
        public string friendlies { get; set; }
    }

    class WclRequest
    {
        static private string apikey = "?api_key=749c8147119cef932af88b44a898af00";
        static HttpClient client = new HttpClient();

        /*static void DisplayAttendance(WarcraftLog wcl)
        {
            Console.WriteLine($"ID: {wcl.id}\tTitle: {wcl.owner}\tTitle: {wcl.title}");
        }*/

        public static async Task<ImportLogObj> wcl(string logCode)
        {
            string url = "https://www.warcraftlogs.com:443/v1/report/fights/" + logCode + apikey;
            var response = await client.GetStringAsync(url);
            var result = ImportLogObj.FromJson(response);
            //var resp = JsonConvert.DeserializeObject<WarcraftLog>(response);
            
            return result;
        }
    }
}