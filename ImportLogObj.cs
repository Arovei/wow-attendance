﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WoWAttendance
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class ImportLogObj
    {
        [JsonProperty("fights")]
        public List<ImportLogObjFight> Fights { get; set; }

        [JsonProperty("friendlies")]
        public List<Friendly> Friendlies { get; set; }

        [JsonProperty("zone")]
        public long Zone { get; set; }
    }
    public partial class ImportLogObjFight
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("kill", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Kill { get; set; }
    }
    public partial class Friendly
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("guid")]
        public long Guid { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("fights")]
        public List<EnemyPetFight> Fights { get; set; }
    }
    public partial class EnemyPetFight
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("instances", NullValueHandling = NullValueHandling.Ignore)]
        public long? Instances { get; set; }
    }
    public partial class ImportLogObj
    {
        public static ImportLogObj FromJson(string json) => JsonConvert.DeserializeObject<ImportLogObj>(json, WoWAttendance.Converter.Settings);
    }
    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                //TypeEnumConverter.Singleton,
                //RegionConverter.Singleton,
                //ServerConverter.Singleton,
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }

}
